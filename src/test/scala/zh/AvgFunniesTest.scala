package zh

import org.scalatest.flatspec.AnyFlatSpec

import ZH._

class AvgFunniesTest extends AnyFlatSpec {

  "AvgFunnies" should "konstansokra " in {
    val inputs = Vector( (1, 10, 4.75), (1, 100, 49.913043), (10, 50, 29.526315), (-10, 10, 0.125), (2, 3, 0.0) )
    for (input <- inputs) { 
      val res = avgFunnies(input._1, input._2)
      val actual = input._3
      assert(Math.abs(res - actual) < 0.001, s"${input._1} és ${input._2} közti funny numberek átlaga $actual, az eredmény ${res} lett")
    }
  }
}
