package zh

import org.scalatest.flatspec.AnyFlatSpec

import ZH._

class CountOddFunniesTest extends AnyFlatSpec {

  "CountOddFunnies" should "konstansokra " in {
    val inputs = Vector( (1, 10, 3), (1, 100, 24), (10, 50, 9), (-10, 10, 5) )
    for (input <- inputs) { 
      val res = countOddFunnies(input._1, input._2)
      val actual = input._3
      assert(res == actual, s"${input._1} és ${input._2} közt $actual páratlan funny number van, az eredmény ${res} lett")
    }
  }
}
