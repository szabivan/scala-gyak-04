package zh

import org.scalatest.flatspec.AnyFlatSpec

import ZH._
import scala.util.Random

class DigitTest extends AnyFlatSpec {

  "Digit" should "generáltra " in {
    for ( _ <- 1 to 100) {
      val k = Random.nextInt(7) + 2
      val d = Random.nextInt(k - 1) + 1
      val digits = Random.nextInt(8)+1
      val (actual, n) = (1 to digits).foldLeft[(Int,Int)]((0,0))( (pair, _) => {
        val (count, num) = pair
        val digit = Random.nextInt(k)
        if (digit == d) (count + 1, num * k + digit)
        else (count, num * k + digit)
      })
      val res = countDigits(n, k, d)
      assert(res == actual, s"countDigits($n, $k, $d) == $res, pedig $actual kéne legyen");
    }
  }
}
