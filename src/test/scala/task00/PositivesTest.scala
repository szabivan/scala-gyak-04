package task00

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random
import Lists._

class PositivesTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }

  "positives" should "pontosan a pozitív elemeket kellene megtartsa" in {
    def generateTestCase: (List[Int],List[Int]) = Random.nextInt(10) match {
      case 0 => (Nil,Nil)
      case _ => {
        val head = Random.nextInt( 20 ) - 10
        val (list1, list2) = generateTestCase
        if( head > 0 ) (head::list1, head::list2) else (head::list1, list2)
      }
    }
    for (i <- 1 to 1000) {
      val (test, result) = generateTestCase
      assert( positives( test ) == result , "teszteset: " + test )
  }}

}

