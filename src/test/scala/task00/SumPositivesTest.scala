package task00

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random
import Lists._

class SumPositivesTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }

  "sumPositives" should "azt kellene visszaadja, hogy mennyi a lista pozitív elemeinek az üsszege" in {
    def generateTestCase: (List[Int], Int) = Random.nextInt(10) match {
      case 0 => (Nil,0)
      case _ => {
        val head = Random.nextInt(20) - 10
        val (tail, sum) = generateTestCase
        if( head > 0 ) (head::tail, sum+head) else (head::tail, sum)
      }
    }
    for( i <- 1 to 1000 ) {
      val (testCase, result) = generateTestCase
      assert(sumPositives(testCase) == result, "teszteset: " + testCase )
    }
  }

}

