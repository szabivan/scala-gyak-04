package task00

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random
import Lists._

class KovetLoptakTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }

  "kovet loptak" should "azt kellene visszaadnia, hogy nagyobb-egyenlő-e a lista összege a célszámnál" in {
    def generateTestCase: (List[Int], Int) = Random.nextInt(10) match {
      case 0 => (Nil,0)
      case _ => {
        val head = Random.nextInt(10)
        val (tail, sum) = generateTestCase
        (head::tail, sum+head)
      }
    }
    for( i <- 1 to 1000 ) {
      val diff = Random.nextInt(20) - 10
      val (test, sum) = generateTestCase
      assert(kovetLoptak(test, sum + diff) == (diff <= 0), "teszteset: " + test + ", c=" + (sum+diff) )
    }
  }

}

