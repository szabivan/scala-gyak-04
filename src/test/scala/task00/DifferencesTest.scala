package task00

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random
import Lists._

class DifferencesTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }

  "differences" should "ki kellene adnia a különbségek listáját" in {
    def generateTestCase: (List[Int], List[Int] ) = Random.nextInt(10) match {
      case 0 => (Nil,Nil)
      case _ => {
        val (tail, differences) = generateTestCase
        val head = Random.nextInt(10)
        tail match {
          case Nil => (head::Nil, Nil)
          case head2 :: tail2 => (head::tail, (head2-head)::differences)
        }
      }
    }
    for( i <- 1 to 1000 ) {
      val (testCase, result) = generateTestCase
      assert(differences(testCase) == result , "teszteset: " + testCase )
    }
  }


}

