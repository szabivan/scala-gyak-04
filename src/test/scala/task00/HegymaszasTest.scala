package task00

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random
import Lists._

class HegymaszasTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }

  "hegymaszas" should "vissza kéne adja a pozitív különbségek összegét" in {
    def generateTestCase: (List[Int], Int) = Random.nextInt(5) match {
      case 0 => (Nil, 0)
      case _ => {
        val head = Random.nextInt(10) - 5
        val (tail, sum) = generateTestCase
        tail match {
          case Nil => ( head::Nil, 0 )
          case head2 :: tail2 => ( head::tail, if(head < head2) (head2-head) + sum else sum )
        }
      }
    }
    for( i <- 1 to 1000 ) {
      val (testCase, result) = generateTestCase
      testCase match {
        case head :: _ if head > 0 => assert( hegymaszas(testCase) == result + head, "input: "+testCase )
        case _ => assert( hegymaszas(testCase) == result, "input: "+testCase )
      }
    }
  }

}

