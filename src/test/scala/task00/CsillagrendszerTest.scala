package task00

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random
import Lists._

class CsillagrendszerTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }


  "csillagrendszer" should "true-t kéne visszaadjon, ha az input true,true,..,true,false,false,..,false" in {
    def generateConstant(v: Boolean) : List[Boolean] = Random.nextInt(5) match {
      case 0 => Nil
      case _ => v :: generateConstant( v )
    }
    def generateRandom: List[Boolean] = Random.nextInt(5) match {
      case 0 => Nil
      case _ => Random.nextBoolean() :: generateRandom
    }
    val kozetPrefix = generateConstant(true)
    val gazPrefix = generateConstant(false)
    val crazySuffix = if (gazPrefix.isEmpty) Nil else generateRandom
    assert( valodiCsillagrendszer( kozetPrefix ::: gazPrefix ::: crazySuffix ) == (!crazySuffix.contains(true)), "teszteset: " + (kozetPrefix ::: gazPrefix ::: crazySuffix ))
  }
}

