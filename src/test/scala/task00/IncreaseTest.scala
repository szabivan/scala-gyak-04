package task00

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random
import Lists._

class IncreaseTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }


  "increase" should "növelnie kéne a lista minden elemét c-vel" in { for ( i <- 1 to 1000 ) {
    val c = Random.nextInt(10) - 5
    def generateTestCase: List[(Int,Int)] = Random.nextInt(10) match {
      case 0 => Nil
      case _ => { val head = Random.nextInt(100); (head,head+c)::generateTestCase }
    }
    val combinedList = generateTestCase
    assert( increase( combinedList.map{ _._1 },c) == combinedList.map{ _._2 }, "teszteset: " + combinedList.map{_._1} + ", c=" + c )
  }}


}

