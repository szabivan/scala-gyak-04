package task00

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random
import Lists._

class LeanderTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }


  "viraglocsolas" should "hányat fordul a viráglocsolóember?" in {
    def generateTestCase(capacity: Int): (List[Int], Int ) = Random.nextInt(10) match {
      case 0 => (Nil, 0)
      case _ => {
        val head = Random.nextInt( capacity * 5 )
        val (tail, sum) = generateTestCase(capacity)
        (head::tail, sum + (head + capacity - 1)/capacity)
      }
    }
    for( i <- 1 to 1000 ) {
      val capacity = Random.nextInt( 10 ) + 1
      val (testCase, result) = generateTestCase(capacity)
      assert(viragLocsolas(testCase,capacity) == result, "teszteset: " + testCase + ", capacity: " + capacity )
    }
  }
}

