package task00

object Megoldas extends App {
  /**
   *  Írjunk függvényt, ami a bejövő list Int lista minden elemét megnöveli c-vel!
   *  pl. ha
   *  list = (1,4,2,8,5,7)  és c = 3
   *  akkor az eredmény
   *  (4,7,5,11,8,10)
   */
  def increase(list: List[Int], c: Int): List[Int] = list map { _ + c }

  println( increase( 1::2::3::Nil , 3 ) ) //should print List(4,5,6)

  /**
   * Írjunk függvényt, ami a bejövő list Int lista elemeiből csak a pozitívakat tartja meg!
   * pl. ha
   * list = (1, -4, 2, 0, 8, -5, -7)
   * akkor az eredmény
   * (1,2,8)
   */
  def positives(list: List[Int]): List[Int] = list filter { _ > 0 }

  /**
   * Írjunk függvényt, ami visszaadja, hogy a bejövő Int listáinak az elemeinek összege
   * nagyobb vagy egyenlő-e, mint a bejövő c célszám!
   * pl. ha
   * list = (1, 4, 2, 8, 5, 7) és c = 25
   * akkor az eredmény
   * true
   * mert a lista elemeinek összege 27, ami több, mint 25
   */
  def kovetLoptak(list: List[Int], c: Int):Boolean = list.sum >= c

  /**
   * Írjunk függvényt, ami visszaadja a bejövő Int lista pozitív elemeinek az összegét!
   * pl. ha
   * list = (1, -4, 2, 0, 8, -5, -7)
   * akkor az eredmény
   * 11
   */
  def sumPositives(list: List[Int]): Int = positives(list).sum

  /**
   * Egy sokemeletes toronyban Leandereket kell locsolni, mert csak.
   * A toronyban a lift csak a földszintről egy emeletre és vissza tud közlekedni, és víz csak a földszinten van.
   * Írjunk függvényt, ami paraméterként megkapja egy list: List[Int]-ben, hogy melyik emeleten hány virág van
   * és kap egy capacity: Int kapacitást is, és kiszámolja, hogy hányat kell fordulni, hogy meglocsolják az összes
   * leandert!
   * pl. ha
   * list = (1,4,2,8,5,7) és capacity = 3
   * akkor az eredmény
   * 1 + 2 + 1 + 3 + 2 + 3 = 12
   */
  def viragLocsolas(list: List[Int], capacity: Int): Int =
    list
    .map(n => (n + capacity - 1) / capacity)
    .sum

  /**
   * Egy bejövő list: List[Int]-ből állítsuk elő a szomszédos elemek különbségeit tároló listát!
   * pl. ha
   * list = (1,4,2,8,5,7)
   * akkor az eredmény
   * (3,-2,6,-3,2)
   * Üres listára az eredmény legyen üres lista. (Egyeleműre is az kell legyen.)
   * 
   * List-re is lehet matchelni, így:
   * list match {
   *   case Nil => // eredmény, ha üres a lista
   *   case head :: tail => // eredmény, ha a lista nemüres, első eleme head, lista maradék része tail
   * }
   */
  def differences(list: List[Int]): List[Int] = list match {
    case Nil => Nil
    case head :: Nil => Nil
    case first :: second :: tail => (second - first) :: differences(second :: tail)
  }
  // így is lehet:
  // list.tail kivételt dob az üres listán --> arra külön matchelünk
  // két lista zipje párosítja az elemeiket a rövidebb lista hosszáig
  // pl. List(1, 3, 5, 7).zip(List("dinnye", "szilva", "narancs"))
  // == List((1, "dinnye"), (3, "szilva"), (5, "narancs"))
  // list.zip(list.tail) a szomszédos elemekből csinál tuplékat egy listbe
  // pl. ha list = List(1, 3, 5, 7), akkor list.zip(list.tail) = ((1,3),(3,5),(5,7))
  def differences2(list: List[Int]): List[Int] = list match {
    case Nil => Nil
    case _ => (list zip list.tail) map (pair => pair._2 - pair._1)
  }

  /**
   * Hegymászó ismerősünk végigmászik egy hegyláncon, a magasságokat egy list Int listában kapja meg.
   * Az érdekli, hogy mennyit mászott összességében felfelé (a tengerszint 0-ról indult eredetileg)?
   * pl. ha
   * list = (1,4,2,8,5,7)
   * akkor az eredmény
   * 1 + 3 + 6 + 2 = 12
   * (mert a többi szakaszon lefelé mászott)
   */
  
  def hegymaszas(list: List[Int]): Int =
    sumPositives(differences(0 :: list))
    
  // így is lehet:
  // függvényt így is lehet deklarálni
  // note: így deffel is, vallal is lehet -- mivel maga a függvény ugyanaz, célszerűbb vallal
  val prependZero: List[Int] => List[Int] = 0 :: _
  // f andThen g: függvények kompozíciója, g(f(_))
  // note: ha f(x: Int): Int alakban van adva, akkor kell a neve mögé _ hogy forduljon
  // ha f: Int => Int alakban, akkor nem kell
  // nem hivatkozunk a paraméterre, el sincs nevezve -- "tacit programming" vagy "point-free style"
  val hegymaszas2: List[Int] => Int = prependZero andThen differences _ andThen sumPositives  
  // lehet lambdával is, de akkor meg kell adni a típust h forduljon
  // és kapcsosba kell rakni, nem kerekbe
  val hegymaszas3: List[Int] => Int =
     { list: List[Int] => 0 :: list } andThen differences _ andThen sumPositives   
    
  /**
   * Csillagász ismerősünk csillagrendszereket vizsgál: egy listába feljegyzi, hogy a bolygók
   * belülről kifelé haladva kőzetbolygók (true) vagy gázbolygók (false). Viszont nem emlékszik,
   * hogy a csillagrendszereket tényleg látta, vagy csak álmodta. Egy egyszerű, de hatásos szűrő
   * a következő: egy csillagrendszer valódi, ha nulla, egy vagy több kőzetbolygó van belül, majd kifelé
   * haladva ezeket nulla, egy vagy több gázbolygó követi.
   * Írjunk függvényt, ami megmondja csillagász ismerősünknek, hogy a listájában szereplő rendszer valódi-e!
   * pl. ha
   * list = (true, false, true)
   * akkor az eredmény false
   *
   * ha
   * list = (true, true, false, false, false) vagy
   * list = (true, true, true) vagy
   * list = (false, false)
   * akkor az eredmény true
   */
  // automata (state, input) match
  // state: 0: true*  1: true*false+ 2: .* false true .*
  // note: van warning, hogy a match nem exhaustive (mert nem tudja, hogy az első int mindig 0, 1 vagy 2 lesz)
  def valodiCsillagrendszer(list: List[Boolean]): Boolean =
    list.foldLeft(0)((acc, current) => (acc, current) match {
      case (2, _) => 2
      case (_, false) => 1
      case (0, true)  => 0
      case (1, true) => 2
    }) != 2

}
