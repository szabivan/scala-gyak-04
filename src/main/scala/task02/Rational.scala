package task02

import scala.annotation.tailrec

/**
 * Próbálj idiomatikus Scala kódot írni, pl. ne használj mutable változókat (var), sem returnt.
 * 
 * Ebben a taskban a lentebb látható Rat(szamlalo: Int, nevezo: Int) egy olyan case class, amiben
 * racionális számokat, szamlalo / nevezo alakúakat tárolunk.
 *
 * Írd meg a négy alapműveletet rájuk! Ebben a feladatban még nem kell a hibakezeléssel foglalkozni.
 * 
 */

case class Rat(szamlalo: Int, nevezo: Int)

object Rational extends App {

   def add(a: Rat, b: Rat): Rat = ???
   
   def sub(a: Rat, b: Rat): Rat = ???

   def mul(a: Rat, b: Rat): Rat = ???
   
   def div(a: Rat, b: Rat): Rat = ???
   
   def ellentett(a: Rat): Rat = ???
   
   def isPositive(a: Rat): Boolean = ???
}
