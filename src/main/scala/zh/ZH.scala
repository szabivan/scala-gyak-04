package zh

/**
 * Az alábbi feladatokra próbáljunk meg minél idiomatikusabb Scala kódot írni!
 * Pl.
 * - ne használjunk vart, sem returnt;
 * - preferáljuk a minél kevesebb metódusból álló chaineléseket;
 * - ahol lehet (foreach, filter stb.), használjunk for comprehensionöket;
 * - ha helper függvényt írunk, azt rejtsük el külső szemlélő elől;
 * - rekurzív függvényt próbáljunk tail rekurzívra írni és használni a @tailrec annotációt;
 * - ha egy kifejezést többször számolnánk ki, akkor alakítsuk át a kódunkat úgy, hogy csak egyszer számoljuk ki!
 */
 
 object ZH extends App {
   // az isFunnyNumber predikátumhoz ne nyúljunk, ez a feladatokhoz fog kelleni
   def isFunnyNumber(n: Int) = (n % 3 == 0) || (n % 5 == 1)
   
   /** Írjunk függvényt, ami kap egy from és egy to Intet, és visszaadja, hogy
    *  hány olyan from <= n <= to páratlan egész szám van, melyre igaz az isFunnyNumber predikátum!
    */
   def countOddFunnies(from: Int, to: Int): Int = ???
   
   /**
    * Írjunk függvényt, ami kap egy from és egy to Intet, és visszaadja, hogy
    * mennyi a from <= n < to intervallumban lévő olyan számok **átlaga**, melyekre
    * igaz az isFunnyNumber predikátum! 
    * Ha az intervallumban nincs ilyen szám, adjunk vissza 0.0-t.
    */
    def avgFunnies(from: Int, to: Int): Double = ???
    
    /**
     * Írjunk függvényt, ami kap egy n >= 0, egy k >= 2 és egy 0 <= d < k Int-et
     * és visszaadja, hogy az n szám k-as számrendszerbeli alakjában hány d jegy van!
     * pl. countDigits(100, 3, 0) == 2, mert a 100 szám 3-as számrendszerben 10201.
     * (memó: a számjegyeket rekurzióval és a %, / műveletekkel elő lehet állítani,
     *  pl 10-es számrendszerben a 123-ra 123 % 10 == 3, 123 / 10 == 12,
     *  aztán 12 % 10 == 2, 12 / 10 == 1,
     *  végül 1 % 10 == 1 és 1 / 10 == 0) 
     */
     def countDigits(n: Int, k: Int, d: Int): Int = ???
 }
